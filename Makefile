#!/usr/bin/make -f

all: hpcbio_mash_minhash_jun_2016.pdf

R ?= R

%.pdf: %.svg
	inkscape -A $@ $<
	pdfcrop $@
	mv $(dir $@)*-crop.pdf $@

%.png: %.svg
	inkscape -e $@ -d 300 $<

%.tex: %.Rnw
	$(R) --encoding=utf-8 -e "library('knitr'); knit('$<')"

hpcbio_mash_minhash_jun_2016.pdf: hpcbio_mash_minhash_jun_2016.tex mash_minhash_paper figures

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -pdf -pdflatex='xelatex -interaction=nonstopmode %O %S' -bibtex -use-make $<

